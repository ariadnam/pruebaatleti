#!/usr/bin/python3

from sys import argv


def factorial(n):
    fact = 1
    while n > 1:
        fact = fact * n
        n = n - 1
    return fact


# MAIN EN PYTHON
if __name__ == "__main__":
    for i in range(1, 11):
        print(factorial(i))
